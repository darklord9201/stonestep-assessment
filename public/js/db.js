//offline data
db.enablePersistence()
  .catch(err => {
    if (err.code === 'failed-precondition') {
      console.log('persistence failed')
    } else if (err.code === 'unimplemented') {
      console.log('persistence not available')
    }
  });


// real-time database change listener
db.collection('users').onSnapshot((snapshot) => {
  // console.log(snapshot.docChanges());
  snapshot.docChanges().forEach(change => {
    // console.log(change, change.doc.data());
    if (change.type === 'added') {
      renderUser(change.doc.data(), change.doc.id);
    }

    if (change.type === 'removed') {
      removeUser(change.doc.id);
    }
  })
});

//add new user
const form = document.querySelector('form');
form.addEventListener('submit', evt => {
  evt.preventDefault();

  const user = {
    name: form.name.value,
    contact_no: form.contact_no.value,
    citizenship_no: form.citizenship_no.value,
    address: form.address.value
  }

  db.collection('users').add(user)
    .catch(err => console.log(err));


  form.name.value = '';
  form.contact_no.value = '';
  form.citizenship_no.value = '';
  form.address.value = '';

  M.toast({html: 'User Created Successfully'})
});

//Delete User
const userContainer = document.querySelector('.users');
userContainer.addEventListener('click', evt => {
  if (evt.target.className === 'material-icons delete') {
    const id = evt.target.getAttribute('data-id');
    db.collection('users').doc(id).delete();
    M.toast({html: 'User Deleted Successfully'})
  }
});

