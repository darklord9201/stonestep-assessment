const getWeatherData = () => {
  const weather = document.querySelector('.weather');
  weather.innerHTML = 'Loading Weather ...';
  const weatherDataurl = 'https://nepal-weather-api.herokuapp.com/api/?place=kathmandu';

  fetch(weatherDataurl)
    .then(response => response.json())
    .then(data => {
      console.log(data)
      weather.innerHTML = data.place + ' ' + data.max + '/' + data.min;
    })
    .catch(err => {
      console.log(err)
      weather.innerHTML = 'Error Fetching Weather'
    });
};

getWeatherData();