const users = document.querySelector('.users');

document.addEventListener('DOMContentLoaded', function () {
  // nav menu
  const menus = document.querySelectorAll('.side-menu');
  M.Sidenav.init(menus, {edge: 'right'});

  // add recipe form
  const forms = document.querySelectorAll('.side-form');
  const sideNav = M.Sidenav.init(forms, {edge: 'left'});
});


// render user data
const renderUser = (data, id) => {
  const html = `
    	<div class="card-panel user white row" data-id="${id}">
		    <img src="/img/icons/user.png" alt="user thumb">
		    <div class="user-details">
			    <div class="user-title">${data.name}</div>
			    <div class="user-details"><span>Phone No.:</span> ${data.contact_no} | <span>Citizenship No.:</span> ${data.citizenship_no} | <span>Address:</span> ${data.address}</div>
		    </div>
        <div class="user-delete">
          <i class="material-icons delete" data-id="${id}" style="cursor: pointer">delete_outline</i>
        </div>
	    </div>
  `;

  users.innerHTML += html;
};

// remove user data
const removeUser = (id) => {
  const user = document.querySelector(`.user[data-id=${id}]`);
  user.remove();
};