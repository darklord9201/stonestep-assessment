const staticCacheName = 'site-static-v1.0.0';
const dynamicCacheName = 'site-dynamic-v1.0.0';
const assets = [
  '/',
  '/index.html',
  '/js/app.js',
  '/js/ui.js',
  '/js/weather.js',
  '/js/materialize.min.js',
  '/css/styles.css',
  '/css/materialize.min.css',
  '/img/icons/user.png',
  'https://fonts.googleapis.com/icon?family=Material+Icons',
  'https://fonts.gstatic.com/s/materialicons/v90/flUhRq6tzZclQEJ-Vdg-IuiaDsNcIhQ8tQ.woff2',
  'pages/fallback.html',
];

const weatherCacheName = 'site-weather-data';
const weatherApi = 'https://nepal-weather-api.herokuapp.com';

const limitCacheSize = (name, size) => {
  caches.open(name).then(cache => {
    cache.keys().then(keys => {
      if (keys.length > size) {
        cache.delete(keys[0]).then(limitCacheSize(name, size))
      }
    })
  })
};

self.addEventListener('install', evt => {
  evt.waitUntil(
    caches.open(staticCacheName).then(cache => {
      cache.addAll(assets);
    })
  );
});


self.addEventListener('activate', evt => {
  console.log('service worker activated')
  evt.waitUntil(
    caches.keys().then(keys => {
      return Promise.all(keys
        .filter(key => key !== staticCacheName)
        .map(key => caches.delete(key))
      )
    })
  )
});

self.addEventListener('fetch', evt => {
  if (evt.request.url.indexOf('firestore.googleapis.com') === -1 && evt.request.url.indexOf(weatherApi) === -1) {
    evt.respondWith(
      caches.match(evt.request).then(cacheRes => {
        return cacheRes || fetch(evt.request).then(fetchRes => {
          return caches.open(dynamicCacheName).then(cache => {
            cache.put(evt.request.url, fetchRes.clone());
            limitCacheSize(dynamicCacheName, 30)
            return fetchRes;
          })
        })
      }).catch(() => {
        if (evt.request.url.indexOf('.html') > -1) {
          return caches.match('/pages/fallback.html')
        }
      })
    );
  }

  if (evt.request.url.indexOf(weatherApi) === 0) {
    evt.respondWith(
      caches.match(evt.request).then(cacheRes => {
        if (cacheRes) {
          fetch(evt.request).then((fetchRes) => {
            if (fetchRes.status === 200) {
              return caches.open(weatherCacheName).then((cache) => {
                cache.put(evt.request, fetchRes.clone());
                return fetchRes;
              })
            }

            return fetchRes;
          });

          return cacheRes;
        } else {
          return fetch(evt.request).then(fetchRes => {
            return caches.open(weatherCacheName).then(cache => {
              cache.put(evt.request.url, fetchRes.clone());
              return fetchRes;
            })
          })
        }
      })
    );


    // caches.match(evt.request).then(cacheRes => {
    //   if(cacheRes){
    //     fetch(evt.request).then((fetchRes) => {
    //       return caches.open(weatherCacheName).then((cache) => {
    //         cache.put(evt.request, fetchRes.clone());
    //         return fetchRes;
    //       })
    //     });
    //
    //     return cacheRes;
    //   }else{
    //     return fetch(evt.request).then((fetchRes) => {
    //       return caches.open(weatherCacheName).then((cache) => {
    //         cache.put(evt.request, fetchRes.clone());
    //
    //         return fetchRes;
    //       })
    //     });
    //   })

    //   fetch(evt.request).then(fetchRes => {
    //     return caches.open(weatherCacheName).then(cache => {
    //       cache.put(evt.request.url, fetchRes.clone());
    //       return fetchRes;
    //     })
    //   }).catch(function () {
    //     return caches.match(evt.request);
    //   })
    // );
  }

});